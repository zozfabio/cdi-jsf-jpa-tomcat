package br.com.example.app.domain.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by zozfabio on 24/10/2016.
 */
@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
@ToString(of = {"id", "name"})
public class Person implements Serializable {

    private static final byte serialVersionUID = 1;

    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    public static Person of() {
        return new Person();
    }
}
