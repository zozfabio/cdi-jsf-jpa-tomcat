package br.com.example.app.domain.repository;

import br.com.example.app.domain.model.Person;
import br.com.example.transaction.Transactional;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

import static java.util.Objects.isNull;

/**
 * Created by zozfabio on 24/10/2016.
 */
@ApplicationScoped
public class PersonRepository {

    @Inject
    private EntityManager em;

    @Transactional
    public List<Person> findAll() {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Person> query = builder.createQuery(Person.class);
        Root<Person> root = query.from(Person.class);
        query.select(root);
        List<Person> all = em.createQuery(query).getResultList();
        return all;
    }

    @Transactional
    public Person findOne(Integer id) {
        Person one = em.find(Person.class, id);
        return one;
    }

    @Transactional
    public Person save(Person person) {
        if (isNull(person.getId())) {
            em.persist(person);
        } else {
            em.merge(person);
        }
        return person;
    }

    @Transactional
    public void delete(Integer id) {
        em.remove(findOne(id));
    }
}
