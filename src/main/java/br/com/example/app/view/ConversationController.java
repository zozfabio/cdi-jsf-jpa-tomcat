package br.com.example.app.view;

import lombok.Getter;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by zozfabio on 24/10/2016.
 */
@Named
@ConversationScoped
public class ConversationController implements Serializable {

    private static final byte serialVersionUID = 1;

    @Inject
    private Conversation conversation;

    @Getter
    private Integer count = 0;

    @PostConstruct
    private void constructor() {
        System.out.println("Conversation Controller Init!");
    }

    public String start() {
        conversation.begin();
        return "/conversation";
    }

    public void plusOne() {
        count++;
    }

    public String end() {
        conversation.end();
        return "/index?faces-redirect=true";
    }
}
