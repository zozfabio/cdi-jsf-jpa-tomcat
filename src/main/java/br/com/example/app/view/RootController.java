package br.com.example.app.view;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ResourceBundle;

/**
 * Created by zozfabio on 24/10/2016.
 */
@Named
@ApplicationScoped
public class RootController implements Serializable {

    private static final byte serialVersionUID = 1;

    public String getMessage() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        ResourceBundle rb = ctx.getApplication().getResourceBundle(ctx, "i18n");
        return rb.getString("app.message");
    }
}
