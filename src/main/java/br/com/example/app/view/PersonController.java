package br.com.example.app.view;

import br.com.example.app.domain.model.Person;
import br.com.example.app.domain.repository.PersonRepository;
import lombok.Getter;
import lombok.Setter;

import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by zozfabio on 24/10/2016.
 */
@Named
@ConversationScoped
public class PersonController implements Serializable {

    private static final byte serialVersionUID = 1;

    @Inject
    private Conversation conversation;

    @Inject
    private PersonRepository repository;

    @Getter
    @Setter
    private Integer id;

    @Getter
    private Person model;

    @Getter
    private List<Person> list = new LinkedList<>();

    public String list() {
        if (conversation.isTransient()) {
            conversation.begin();
        }
        list = repository.findAll();
        return "/person/list?faces-redirect=true";
    }

    public String create() {
        model = Person.of();
        return "/person/create?faces-redirect=true";
    }

    public String edit() {
        model = repository.findOne(id);
        return "/person/edit?faces-redirect=true";
    }

    public String save() {
        repository.save(model);
        return list();
    }

    public String delete() {
        repository.delete(id);
        return list();
    }

    public String end() {
        conversation.end();
        return "/index?faces-redirect=true";
    }
}
