package br.com.example.app.view;

import lombok.Getter;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by zozfabio on 24/10/2016.
 */
@Named
@ApplicationScoped
public class SingletonController implements Serializable {

    private static final byte serialVersionUID = 1;

    @PostConstruct
    private void constructor() {
        System.out.println("Singleton Controller Init!");
    }

    @Getter
    private Integer count = 0;

    public String start() {
        return "/singleton";
    }

    public void plusOne() {
        count++;
    }

    public String end() {
        return "/index?faces-redirect=true";
    }
}
