package br.com.example.app.infrastructure.cdi;

import org.jboss.weld.bean.ManagedBean;
import org.jboss.weld.bean.ProducerMethod;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zozfabio on 25/10/2016.
 */
public class EagerExtension implements Extension {

    private List<Bean<?>> eagerBeansList = new ArrayList<>();

    public <T> void collect(@Observes ProcessBean<T> event) {
        if (event.getAnnotated().isAnnotationPresent(ApplicationScoped.class)) {
            eagerBeansList.add(event.getBean());
        }
    }

    public void load(@Observes AfterDeploymentValidation event, BeanManager beanManager) {
        for (Bean<?> bean : eagerBeansList) {
            CreationalContext<?> creationalContext = beanManager.createCreationalContext(bean);

            if (bean instanceof ProducerMethod) {
                ProducerMethod<?, ?> producerMethod = (ProducerMethod) bean;
                beanManager.getReference(producerMethod, producerMethod.getType(), creationalContext).toString();
            } else if (bean instanceof ManagedBean) {
                ManagedBean<?> managedBean = (ManagedBean) bean;
                beanManager.getReference(managedBean, managedBean.getType(), creationalContext).toString();
            }
        }
    }
}
