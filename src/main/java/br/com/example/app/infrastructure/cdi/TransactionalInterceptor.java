package br.com.example.app.infrastructure.cdi;

import br.com.example.transaction.Transactional;

import javax.enterprise.inject.spi.CDI;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * Created by zozfabio on 26/10/2016.
 */
@Interceptor
@Transactional
public class TransactionalInterceptor {

    @AroundInvoke
    public Object runInTransaction(InvocationContext invocationContext) throws Exception {
        EntityManager em = CDI.current()
                .select(EntityManager.class).get();

        EntityTransaction transaction = em.getTransaction();

        try {
            transaction.begin();

            Object result = invocationContext.proceed();

            transaction.commit();

            return result;
        } catch (Exception e) {
            try {
                if (transaction.isActive()) {
                    transaction.rollback();
                }
            } catch (Exception e1) {}
            throw e;
        }
    }
}
