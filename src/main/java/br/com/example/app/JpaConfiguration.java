package br.com.example.app;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by zozfabio on 24/10/2016.
 */
@ApplicationScoped
public class JpaConfiguration {

    @Produces
    @ApplicationScoped
    public EntityManagerFactory entityManagerFactory() {
        return Persistence.createEntityManagerFactory("default");
    }

    @Produces
    @ApplicationScoped
    public EntityManager entityManager(EntityManagerFactory emf) {
        return emf.createEntityManager();
    }

    public void close(@Disposes EntityManager em) {
        if (em.isOpen()) {
            em.close();
        }
    }
}
